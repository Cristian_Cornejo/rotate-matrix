package com.cristiancornejo.rotatematrix.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cristiancornejo.rotatematrix.domain.service.impl.RotateMatrixService;

@RestController
@RequestMapping("matrix")
public class RotateMatrixController {

    @Autowired
    private RotateMatrixService rotateArrayService;

    @PostMapping("/rotate")
    public ResponseEntity<Integer[][]> getRotatedArray(@RequestBody Integer matrix[][]) {
        
        Integer rotatedMatrix[][] = rotateArrayService.getRotatedArray(matrix);

        return new ResponseEntity<>(rotatedMatrix, HttpStatus.CREATED);
    }
}
