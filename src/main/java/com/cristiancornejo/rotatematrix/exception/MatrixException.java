package com.cristiancornejo.rotatematrix.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class MatrixException extends RuntimeException {

    public static final String NOT_ONLY_NUMBERS = "No se ingresó solamente números";
    public static final String NOT_SQUARE_MATRIX = "No se ingresó una matriz N x N";
    public static final String NOT_MATRIX = "No se ingresó una matriz";

    public MatrixException() {
        super();
    }
    public MatrixException(String message, Throwable cause) {
        super(message, cause);
    }
    public MatrixException(String message) {
        super(message);
    }
    public MatrixException(Throwable cause) {
        super(cause);
    }
}