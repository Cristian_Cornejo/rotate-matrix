package com.cristiancornejo.rotatematrix.domain.service.impl;

import com.cristiancornejo.rotatematrix.domain.service.IRotateMatrixService;
import com.cristiancornejo.rotatematrix.exception.MatrixException;
import org.springframework.stereotype.Service;

@Service
public class RotateMatrixService implements IRotateMatrixService {

    public Integer[][] getRotatedArray(Integer matrix[][]) {
        Integer n = matrix.length;

        if (n == 0) {
            throw new MatrixException(MatrixException.NOT_MATRIX);
        }

        if (n != matrix[0].length) {
            throw new MatrixException(MatrixException.NOT_SQUARE_MATRIX);
        }
        Integer result[][] = new Integer[n][n];
        try {
            int y = 0;
            for (int i = n - 1; i >= 0; i--) {
                int x = 0;
                for (int j = 0; j <= n - 1; j++) {
                    result[y][x++] = matrix[j][i];
                }
                y++;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new MatrixException(MatrixException.NOT_SQUARE_MATRIX);
        } catch (Exception e) {
            throw new MatrixException(MatrixException.NOT_ONLY_NUMBERS);
        }

        return result;
    }
}
