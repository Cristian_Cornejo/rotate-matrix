package com.cristiancornejo.rotatematrix.domain.service;

public interface IRotateMatrixService {
    Integer[][] getRotatedArray(Integer matrix[][]);
}
