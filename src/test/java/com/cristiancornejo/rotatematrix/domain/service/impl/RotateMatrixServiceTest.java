package com.cristiancornejo.rotatematrix.domain.service.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.cristiancornejo.rotatematrix.exception.MatrixException;

@ExtendWith(MockitoExtension.class)
public class RotateMatrixServiceTest {
    @InjectMocks
    private RotateMatrixService rotateMatrixService;

    @Test
    void getRotatedArrayOk2x2Test() {
        Integer matrix[][] = { { 1, 2 }, { 3, 4 } };
        Integer expected[][] = { { 2, 4 }, { 1, 3 } };
        Integer result[][] = rotateMatrixService.getRotatedArray(matrix);
        Assertions.assertArrayEquals(expected, result);
    }

    @Test
    void getRotatedArrayOk3x3Test() {
        Integer matrix[][] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
        Integer expected[][] = { { 3, 6, 9 }, { 2, 5, 8 }, { 1, 4, 7 } };
        Integer result[][] = rotateMatrixService.getRotatedArray(matrix);
        Assertions.assertArrayEquals(expected, result);
    }

    @Test
    void getRotatedArrayNotSquareMatrixErrorTest() {
        Integer matrix[][] = { { 1, 2 }, { 4, 5 }, { 7, 8 } };

        Assertions.assertThrows(MatrixException.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                rotateMatrixService.getRotatedArray(matrix);
            }
        });
    }
}
